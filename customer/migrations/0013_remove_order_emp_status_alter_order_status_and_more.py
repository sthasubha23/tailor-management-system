# Generated by Django 4.0.4 on 2022-08-18 11:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0012_alter_order_emp_status'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='emp_status',
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(choices=[('Rejected', 'Rejected'), ('Assigned', 'Assigned'), ('Complete', 'Completed')], default='Assigned', max_length=25),
        ),
        migrations.AlterField(
            model_name='ordereddescription',
            name='status',
            field=models.CharField(choices=[('Rejected', 'Rejected'), ('Assigned', 'Assigned'), ('Complete', 'Completed')], default='Not Complete', max_length=25),
        ),
    ]
